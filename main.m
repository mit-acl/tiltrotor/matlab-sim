%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tri-Tiltrotor Simulation
%
% Modeling and control of the eflite convergence tiltrotor
%
% Parker Lusk
% 23 July 2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all, clc
set(0,'DefaultLineLineWidth',1.5); % 0.5
% -------------------------------------------------------------------------
% Simulation Setup

% timing
Tf = 6;   % [s] how long is the simulation
Ts = 0.01; % [s] simulation / control period
N = Tf/Ts; %     number of iterations
tvec = linspace(0,Tf,N);

state.pos = zeros(3,1); % [m]     intertial-frame position
state.vel = zeros(3,1); % [m/s]   inertial-frame velocities
state.q = [1 0 0 0];    % (wxyz)  rotation of body w.r.t world
state.w = [0 0 0]';   % [rad/s] rate of body w.r.t world exprssd in body

% parameters
P.trajDrawPeriod = 0.1;
P.simDrawPeriod = 0.005;
P.Ts = Ts;
P.gravity = 9.80665; % [m/s/s]
P.mass = 0.250; % [kg]
P.J = diag([0.12 0.12 0.12]);
P.spinup = 0.25; % seconds to wait before commanding goal

% set up visualization
figure(1), clf; hold on;
hViz = viz(state, [], [], P);
view(-50,30);

% -------------------------------------------------------------------------
% Main Simulation Loop

statelog = cell(N,1);
inputlog = cell(N,1);
goallog = cell(N,1);

for i = 1:N
    t = tvec(i);
    
    statelog{i} = state;
    
    goal.pos = [1 1 1]';
%     if Ts*i < P.spinup, goal.pos = [0 0 0]';end
    cmd = pidcontroller(state, goal, t, P);
    
    % control allocation
    cmd.actuators = ttr_mixer(cmd.u);
    
    hViz = viz(state, cmd, hViz, P);
    
    % ------------ The Plant -------------
    if Ts*i < Tf/6
        u = [0 0 0.95*P.mass*P.gravity 0 0 0]';
    elseif Ts*i > Tf/6 && Ts*i < 2*Tf/6
        u = [0 0 1.1*P.mass*P.gravity 0 0 0]';    
    else
        u = [0 0 P.mass*P.gravity 0 0 0]';
    end
%     cmd.actuators = ttr_mixer(u);
    cmd.u = invmixer(cmd.actuators);
    state = dynamics(state, cmd.u, Ts, P);
    % ------------------------------------
    
    if mod(i,P.simDrawPeriod/P.Ts) == 0, drawnow; end
    
    inputlog{i} = cmd;
    goallog{i} = goal;
    
    % check for runaways
    if any(abs(state.pos)>100)
        fprintf('\n**Runaway detected**\n\n');
        break;
    end
end

% Plot state log
figure(3), clf; hold on;
plotState(statelog, inputlog, goallog, tvec, P);

% Plot actuator log
figure(4), clf; hold on;
plotActuators(inputlog, tvec, P);

% =========================================================================
% Helpers
% =========================================================================

function plotState(statelog, inputlog, goallog, tvec, P)

states = [statelog{:}];
p = [states.pos]';
v = [states.vel]';
q = reshape([states.q], 4, size(p,1))';
w = [states.w]' * 180/pi;

inputs = [inputlog{:}];
u = [inputs.u]';
Fi = [inputs.Fi]';
qdes = reshape([inputs.qdes], 4, size(p,1))';
qe = reshape([inputs.qe], 4, size(p,1))';
wdes = [inputs.wdes]' * 180/pi;
afb = [inputs.accel_fb]';
jfb = [inputs.jerk_fb]';

goals = [goallog{:}];
pdes = [goals.pos]';
vdes = zeros(3,length(p)); %[goals.vel]';

% quaternion to RPY for plotting
RPY = quat2eul(q,'ZYX') * 180/pi;
RPYdes = quat2eul(qdes,'ZYX') * 180/pi;
RPYerr = quat2eul(qe,'ZYX') * 180/pi;

% in case of early terminations
tvec = tvec(1:length(p));

% color order: make the desired and actual traces the same color
co = get(gca, 'ColorOrder');
set(groot, 'defaultAxesColorOrder', [co(1:3,:); co(1:3,:)]);

n = 6; i = 1;
subplot(n,1,i); i = i+1;
plot(tvec,p); grid on; ylabel('Position'); hold on;
plot(tvec,pdes,'--');
title('State Log'); legend('x','y','z');

subplot(n,1,i); i = i+1;
plot(tvec,v); grid on; ylabel('Velocity'); hold on;
plot(tvec,vdes,'--');

% subplot(n,1,i); i = i+1;
% plot(tvec,q(:,2:4)); grid on; ylabel('Quaternion'); hold on;
% plot(tvec,qdes(:,2:4),'--');

subplot(n,1,i); i = i+1;
plot(tvec,RPY); grid on; ylabel('Euler RPY'); hold on;
plot(tvec,RPYdes,'--');

% subplot(n,1,i); i = i+1;
% plot(tvec,sign(qe(:,1)).*qe(:,2:4)); grid on; ylabel('Quat Error');

% subplot(n,1,i); i = i+1;
% plot(tvec,RPYerr); grid on; ylabel('RPY Error');

subplot(n,1,i); i = i+1;
plot(tvec,w); grid on; ylabel('Body Rates'); hold on;
plot(tvec,wdes,'--');

subplot(n,1,i); i = i+1;
plot(tvec,u(:,1:3)); grid on; ylabel('Forces');

subplot(n,1,i); i = i+1;
plot(tvec,u(:,4:6)); grid on; ylabel('Moments');

% subplot(n,1,i); i = i+1;
% plot(tvec,afb); grid on; ylabel('Accel Feedback');

% subplot(n,1,i); i = i+1;
% plot(tvec,jfb); grid on; ylabel('Jerk Feedback');

xlabel('Time [s]');

end

function plotActuators(inputlog, tvec, P)

inputs = [inputlog{:}];
actuators = [inputs.actuators];
actuators = reshape(actuators, [], length(inputs))';

% in case of early terminations
tvec = tvec(1:length(actuators));

O1 = sqrt(actuators(:,1));
O2 = sqrt(actuators(:,2));
O3 = sqrt(actuators(:,3));

n = 5; i = 1;
subplot(n,1,i); i = i+1;
plot(tvec,O1); grid on; ylabel('M1 [rpm]'); hold on;
title('Actuator Log');

subplot(n,1,i); i = i+1;
plot(tvec,O2); grid on; ylabel('M2 [rpm]'); hold on;

subplot(n,1,i); i = i+1;
plot(tvec,O3); grid on; ylabel('M3 [rpm]'); hold on;

subplot(n,1,i); i = i+1;
plot(tvec,rad2deg(actuators(:,4))); grid on; ylabel('S1 [deg]'); hold on;

subplot(n,1,i); i = i+1;
plot(tvec,rad2deg(actuators(:,5))); grid on; ylabel('S2 [deg]'); hold on;

xlabel('Time [s]');

end
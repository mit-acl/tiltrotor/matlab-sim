%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\documentclass[a4paper]{article}

\usepackage[bottom]{footmisc}
\usepackage{xltxtra}
\usepackage{amsfonts}
\usepackage{polyglossia}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{dsfont}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{physics}
\usepackage{mathtools}
\usepackage{bm}
\usepackage{siunitx}
\usepackage{subcaption}
\usepackage{float}

\usepackage{graphicx}
\graphicspath{ {figures/} }

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    citecolor=blue,
}
\urlstyle{same}

% Hack to make ieeeconf and natbib get along
% http://newsgroups.derkeiler.com/Archive/Comp/comp.text.tex/2006-02/msg00834.html
\makeatletter
\let\NAT@parse\undefined
\makeatother
\usepackage[numbers]{natbib}
\setcitestyle{aysep={}} % remove comma
\usepackage{usebib}
\bibinput{refs}

\geometry{a4paper,left=15mm,right=15mm,top=20mm,bottom=20mm}
\pagestyle{fancy}
\lhead{Parker C. Lusk}
\chead{Tiltrotor}
\rhead{\today}
\cfoot{\thepage}

\setlength{\headheight}{23pt}
% \setlength{\parindent}{0.0in}
\setlength{\parskip}{0.03in}

\newtheorem*{prop}{Proposition}
\newtheorem*{defn}{Definition}
\newtheorem*{thm}{Theorem}
\newtheorem*{cor}{Corollary}
\newtheorem*{lem}{Lemma}
\newtheorem*{rem}{Remark}

\DeclarePairedDelimiterX{\inn}[2]{\langle}{\rangle}{#1, #2}

\begin{document}
\section*{Overview}
In this report, the autopilot control strategy is outlined for a tiltrotor unmanned aerial vehicle (UAV).
The vehicle used is the E-Flite Convergence VTOL (vertical takeoff and landing).

\begin{figure}[h]
\centering
\includegraphics[width=0.50\textwidth]{figures/convergence_topdown_small.png}
\caption{E-Flite Convergence vehicle}
\end{figure}

\section*{Rigid Body Dynamics}
We derive the equations of motions in an inertial East-North-Up (ENU) frame with a body front-left-up (flu) frame.
The nonlinear equations of motion for a 6 DOF rigid body are
\begin{align}
\dot{\mathbf{x}} &= \dot{\mathbf{v}} \\
\dot{\mathbf{v}} &= \frac{1}{\mathsf{m}}\left(\mathbf{R}^\text{w}_\text{b}\mathbf{F} - \mathbf{F}_g\right) \\
\dot{\mathbf{R}}^\text{w}_\text{b} &= \mathbf{R}^\text{w}_\text{b} \mathbf{\omega}_\times \\ % transpose?
\dot{\mathbf{\omega}} &= \mathbf{J}^{-1}\left(\mathbf{M} - \omega\times\mathbf{J}\omega\right).
\end{align}
Position $\mathbf{x}\in\mathbb{R}^3$ and velocity $\mathbf{v}\in\mathbb{R}^3$ are expressed in the inertial ENU frame, the attitude $\mathbf{R}^\text{w}_\text{b}\in\text{SO}(3)$ is the orientation of the body w.r.t the world, and $\omega\in\mathbb{R}^3$ is the angular rate of the body w.r.t the world expressed in the body frame.
The inputs to this system are the forces $\mathbf{F}\in\mathbb{R}^3$ and moments $\mathbf{M}\in\mathbb{R}^3$, both expressed in the body frame.

\section*{Tiltrotor Modeling}
In this section, we model the forces and moments applied to the body from the actuators.

A similar vehicle is found in~\cite{Ansari2017}.

\subsection*{Tilting Mechanism}
The nacelle servos attached to motors 1 and 2 counteract the adverse yaw created by the asymmetry in the rotor spin directions.
These servos are able to tilt the motors from vertical with a range of $-30^\circ \le \alpha_i \le 90^\circ$.
In hover flight, the motors are tilted differentially (i.e., $\alpha_1 = -\alpha_2$) and are limited in the range $|\alpha_i| \le 30^\circ$.

\subsection*{Actuators}
Assuming constant air density, the aerodynamic force and moment produced by the $i^\text{th}$ rotor are given by
\begin{equation}
f_i = k_f\Omega_i^2,\quad m_i = k_m\Omega_i^2,
\end{equation}
where $\Omega_i$ is the speed (rpm) of the $i^\text{th}$ motor and $k_f$, $k_m$ are constants.
Note that this relationship is often captured using thrust stand data.

To understand how the motor actuators create body forces and moments, 
The \textit{actuator effectiveness matrix} maps the actuators to body forces and moments.
To calculate it, we start with considering the forces and moments in the body as functions of the actuators.
\begin{align}
M_x &= (f_2\cos\alpha_2 - f_1\cos\alpha_1)\ell_y + m_2\sin\alpha_2 - m_1\sin\alpha_1 \\
M_y &= f_3\ell_3 - (f_1\cos\alpha_1 + f_2\cos\alpha_2)\ell_x \\
M_z &= m_2\cos\alpha_2 - m_1\cos\alpha_1 - m_3 + (f_1\sin\alpha_1 - f_2\sin\alpha_2)\ell_y \\
F_x &= f_1\sin\alpha_1 + f_2\sin\alpha_2 \\
F_y &= 0 \\
F_z &= f_1\cos\alpha_1 + f_2\cos\alpha_2 + f_3
\end{align}

We can write these equations in terms of the motor speeds as
\begin{align}
M_x &= (\Omega_2^2\cos\alpha_2 - \Omega_1^2\cos\alpha_1)k_f\ell_y + k_m(\Omega_2^2\sin\alpha_2 - \Omega_1^2\sin\alpha_1) \\
M_y &= k_f\Omega_3^2\ell_3 - (\Omega_1^2\cos\alpha_1 + \Omega_2^2\cos\alpha_2)k_f\ell_x \\
M_z &= k_m(\Omega_2^2\cos\alpha_2 - \Omega_1^2\cos\alpha_1 - \Omega_3^2) + (\Omega_1^2\sin\alpha_1 - \Omega_2^2\sin\alpha_2)k_f\ell_y \\
F_x &= k_f(\Omega_1^2\sin\alpha_1 + \Omega_2^2\sin\alpha_2) \\
F_y &= 0 \\
F_z &= k_f(\Omega_1^2\cos\alpha_1 + \Omega_2^2\cos\alpha_2 + \Omega_3^2).
\end{align}

\subsection*{Allocation Strategy}
The chosen allocation strategy used here is similar to \textit{daisy-chaining} and contains two steps: 1) allocate the motors to achieve the desired rolling and pitching moments as well as thrust in the body $z$-direction; 2) allocate the servo differential tilt angle to cancel the adverse yaw moment.
Variations of this strategy can be found in the PX4 firmware and in~\cite{Yu2017,Yu2019}.

Because motors 1 and 2 are differentially tilted in hover flight, when the total force in the $x$-direction is 0, $f_1=f_2$.

The actuator effectiveness matrix for the first step is
\begin{equation}
\underbrace{\begin{bmatrix} M_x \\ M_y \\ F_z \end{bmatrix}}_{\mathbf{m}}
=
\underbrace{\begin{bmatrix}
-k_f\ell_y\cos\alpha_1 -k_m\sin\alpha_1 &  k_f\ell_y\cos\alpha_2 + k_m\sin\alpha_2 & 0         \\
-k_f\ell_x\cos\alpha_1                  & -k_f\ell_x\cos\alpha_2                   & k_f\ell_3 \\
 k_f\cos\alpha_1                        &  k_f\cos\alpha_2                         & k_f
\end{bmatrix}}_{B}
\underbrace{\begin{bmatrix} \Omega_1^2 \\ \Omega_2^2 \\ \Omega_3^2 \end{bmatrix}}_{\mathbf{u}}.
\end{equation}

Once $M_x$, $M_y$, and $F_z$ are allocated (using the current tilt angle, initialized to 0 (?)), we can determine the necessary tilt angle to command the desired yaw moment (and cancel out adverse yaw).
As we are operating at a small angle around verticle, we use $\cos\alpha\approx1$ and $\sin\alpha\approx\alpha$.
Also, recall that $\alpha_2 = -\alpha_1$.

\begin{align}
M_z &= k_m(\Omega_2^2\cos\alpha_2 - \Omega_1^2\cos\alpha_1 - \Omega_3^2) + (\Omega_1^2\sin\alpha_1 - \Omega_2^2\sin\alpha_2)k_f\ell_y \\
M_z &= k_m(\Omega_2^2\cos\alpha_1 - \Omega_1^2\cos\alpha_1 - \Omega_3^2) + (\Omega_1^2\sin\alpha_1 + \Omega_2^2\sin\alpha_1)k_f\ell_y \\
M_z &\approx k_m(\Omega_2^2 - \Omega_1^2 - \Omega_3^2) + (\Omega_1^2\alpha_1 + \Omega_2^2\alpha_1)k_f\ell_y \\
\Rightarrow \alpha_1 &\approx \frac{M_z - k_m(\Omega_2^2 - \Omega_1^2 - \Omega_3^2)}{(\Omega_1^2 + \Omega_2^2)k_f\ell_y}
\end{align}

\section*{Actuator Effectiveness Matrix}
The \textit{actuator effectiveness matrix} maps the actuators to body forces and moments.
To calculate it, we start with considering the forces and moments in the body as functions of the actuators.
\begin{align}
M_x &=  C_t f^1_z\ell_y - C_t f^2_z\ell_y + C_m f^1_x - C_m f^2_x \\
M_y &= -C_t f^1_z\ell_x - C_t f^2_z\ell_x + C_t f^3\ell_b \\
M_z &=  C_m f^1_z - C_m f^2_z - C_m f^3 \\
F_x &=  C_t f^1_x + C_t f^2_x \\
F_y &=  0 \\
F_z &=  C_t f^1_z + C_t f^2_z + C_t f^3
\end{align}
Thus, the actuator effectiveness matrix is
\begin{equation}
\underbrace{\begin{bmatrix} M_x \\ M_y \\ M_z \\ F_x \\ F_z \end{bmatrix}}_{\mathbf{m}}
=
\underbrace{\begin{bmatrix}
C_m & C_t\ell_y & -C_m & -C_t\ell_y & 0 \\
0 & -C_t\ell_x & 0 & -C_t\ell_x & C_t\ell_b \\
0 & C_m & 0 & -C_m & -C_m \\
C_t & 0 & C_t & 0 & 0 \\
0 & C_t & 0 & C_t & C_t
\end{bmatrix}}_{B}
\underbrace{\begin{bmatrix} f^1_x \\ f^1_z \\ f^2_x \\ f^2_z \\ f^3 \end{bmatrix}}_{\mathbf{u}}.
\end{equation}

\section*{Control Allocation}
The attitude controller specifies forces and moments to be applied to the vehicle.
We wish to determine the appropriate actuator response to achieve the desired forces and moments $\mathbf{m}$.
This is performed via \textit{control allocation}.

A common method of control allocation is using the generalized inverse~\cite{Durham1993} (i.e., the Moore-Penrose pseudoinverse).
In the case of redundant actuators, this produces the min-norm solution.
\begin{align}
\mathbf{u} &= B^\dagger\mathbf{m} \\
&= B^\top(BB^\top)^{-1}\mathbf{m} \\
&= A\mathbf{m}
\end{align}
At every control iteration, the \textit{mixing matrix} $A$ can be used to map desired forces and moments into actuator commands.

\subsection*{Solving for Tilt Angle}
Note that $\mathbf{u}$ is in terms of components.
To command the actuators, we perform the following mapping
\begin{align}
f_i &= \left\lVert\begin{bmatrix}f^i_x \\ f^i_z\end{bmatrix}\right\rVert = \sqrt{(f^i_x)^2 + (f^i_z)^2} \\
\alpha_i &= \arctan\frac{f^i_z}{f^i_x},
\end{align}
where $\alpha_i$ is the angle from level (forward flight) of the $i$-th actuator.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{IEEEtranN}
\bibliography{refs}

\end{document}

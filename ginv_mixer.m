function actuators = mixer(u)
% multirotor mode initial actuators
actuators = [0 0 0 pi/2 pi/2];

persistent M;
if isempty(M)
    syms Cm ellx elly ellb cTb;

    A2 = [Cm  elly  -Cm -elly  0;...
          0  -ellx   0  -ellx  ellb;...
          0   Cm     0  -Cm   -Cm ;...
          0   1      0   1     1];

    % evaluate model using measurements of full-size convergence
    ellx = 0.080;
    elly = 0.200;
    ellb = 0.250;
    Cm = 0.2;
    M = double(subs(pinv(A2)));
end

FM = [u(4); u(5); u(6); u(3)];
f = M*FM;
 
f1 = norm([f(1) f(2)]);
a1 = atan2(f(2),f(1));
f2 = norm([f(3) f(4)]);
a2 = atan2(f(4),f(3));
f3 = f(5);

% a1 = sat(a1, 0, pi/2);
% a2 = sat(a2, 0, pi/2);

actuators = [f1 f2 f3 a1 a2];
end

function v = sat(u, min, max)
    if     u>max, v = max;
    elseif u<min, v = min;
    else        , v = u;
    end
end
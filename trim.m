% flu body

% f1x f1z f2x f2z f3 theta phi
x0 = [0.25 0.25 0.25 0.25 0.75 -0.185 0];

% be quiet
opts = optimset('Display','off');

x = fsolve(@trimfun, x0, opts)

f1 = norm(x(1:2));
a1 = atan2d(x(2),x(1));
f2 = norm(x(3:4));
a2 = atan2d(x(4),x(3));

deltad = [f1 f2 x(5) a1 a2]
delta = [f1 f2 x(5) deg2rad(a1) deg2rad(a2)];

function F = trimfun(x)
% motor geometry (m)
ell_L = [ 0.080  0.200  0]';
ell_R = [ 0.080 -0.200  0]';
ell_B = [-0.250  0      0]';
ellx = abs(ell_R(1)); elly = abs(ell_R(2)); ellb = abs(ell_B(1));

m = 0.250;
g = 9.80665;
Ct = 0.3*9.80665; % convert motor PWM command to force
Cm = 0.3; % rotor drag constant

% actuators
f1x = x(1);
f1z = x(2);
f2x = x(3);
f2z = x(4);
f3  = x(5);
th  = x(6);
ph  = x(7);

% force and moments from actuators
Fx =   Ct*f1x + Ct*f2x;
Fy =   0;
Fz =   Ct*f1z + Ct*f2z       + Ct*f3;
Mx = ( Ct*f1z - Ct*f2z)*elly + Cm*f1x - Cm*f2x;
My = (-Ct*f1z - Ct*f2z)*ellx + Ct*f3*ellb;
Mz =   Cm*f1z - Cm*f2z       - Cm*f3;

% add gravity to calculate total force acting on vehicle
Fx = Fx + m*g*sin(th);
Fy = Fy - m*g*cos(th)*sin(ph);
Fz = Fz - m*g*cos(th)*cos(ph);

F = [Mx My Mz Fx Fy Fz]';
end
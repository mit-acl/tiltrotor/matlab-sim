function u = invmixer(actuators)
%INVMIXER Mapping of actuators to forces and moments
%   Note: these forces and moments are in body (flu) frame

% break out actuators
O1sq = actuators(1);
O2sq = actuators(2);
O3sq = actuators(3);
a1 = actuators(4);
a2 = actuators(5);

% aerodynamic force and moment coefficients
kf = 4.531e-5; % N/rpm^2
km = 9.409e-7; % N-m/rpm^2

% motor geometry
ellx = 0.080; % m
elly = 0.200; % m
ell3 = 0.250; % m

% aerodynamic forces and moments from motor speed
f1 = kf*O1sq;
f2 = kf*O2sq;
f3 = kf*O3sq;
m1 = km*O1sq;
m2 = km*O2sq;
m3 = km*O3sq;

% forces and moments in the body frame due to actuators
Mx = (f2*cos(a2) - f1*cos(a1))*elly + m2*sin(a2) - m1*sin(a1);
My = f3*ell3 - (f1*cos(a1) + f2*cos(a2))*ellx;
Mz = m2*cos(a2) - m1*cos(a1) - m3 + (f1*sin(a1) - f2*sin(a2))*elly;
Fx = f1*sin(a1) + f2*sin(a2);
Fy = 0;
Fz = f1*cos(a1) + f2*cos(a2) + f3;

u = [Fx Fy Fz Mx My Mz]';
end


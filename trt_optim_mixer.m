function actuators = trt_optim_mixer(u)
% Omega1 Omega2 Omega3 mu
actuators0 = [100 100 100 0];

% be quiet
opts = optimset('Display','off');

fun = @(actuators) trimfun(actuators, u);
actuators = fsolve(fun,actuators0, opts);
end

function F = trimfun(actuators, u)
% http://www-personal.umich.edu/~dsbaero/library/ConferencePapers/AnsariPrachTiltRotorACC2017.pdf
% See Figure 1 for coordinate frame (frd) --- yes, body-x is out the tail

% tricopter parameters
m = 1.1; % kg
ell1 = 0.2483; % m
ell2 = 0.1241; % m
ell3 = 0.2150; % m
KF = 1.970e-6; % N / rpm^2
KM = 2.880e-7; % N-m / rpm^2

% actuators
O1 = actuators(1);
O2 = actuators(2);
O3 = actuators(3);
mu = actuators(4);

% mix to aerodynamic forces
F1 = KF*O1^2;
F2 = KF*O2^2;
F3 = KF*O3^2;
M1 = KM*O1^2;
M2 = KM*O2^2;
M3 = KM*O3^2;

% desired body-frame forces and moments
Fxd = u(1);
Fyd = u(2);
Fzd = u(3);
Mxd = u(4);
Myd = u(5);
Mzd = u(6);

% LHS of constraint (equal to zero desired)
Mx = -ell3*(F2 - F3);
My = -ell2*(F2 + F3) + ell1*F1*cos(mu);
Mz = ell1*F1*sin(mu) - M1*cos(mu) + M2 - M3;
Fx = 0;
Fy = F1*sin(mu);
Fz = -(F1*cos(mu) + F2 + F3);

% add desired value to constraints
Mx = Mx - Mxd;
My = My - Myd;
Mz = Mz - Mzd;
Fx = Fx - Fxd;
Fy = Fy - Fyd;
Fz = Fz - Fzd;

F = [Mx My Mz Fx Fy Fz]';
end
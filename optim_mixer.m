function actuators = optim_mixer(u)
% multirotor mode initial actuators
actuators0 = [0 0 0 pi/2 pi/2];

% be quiet
opts = optimset('Display','off');

fun = @(actuators) trimfun(actuators, u);
actuators = fsolve(fun,actuators0, opts);
end

function F = trimfun(actuators, u)
% motor geometry (m)
ell_L = [ 0.080  0.200  0]';
ell_R = [ 0.080 -0.200  0]';
ell_B = [-0.250  0      0]';

Ct = 0.3*9.80665; % convert motor PWM command to force
Cm = 0.3; % rotor drag constant

% actuators
f1 = actuators(1);
f2 = actuators(2);
f3 = actuators(3);
a1 = actuators(4);
a2 = actuators(5);

% desired body-frame forces and moments
Fxd = u(1);
Fyd = u(2);
Fzd = u(3);
Mxd = u(4);
Myd = u(5);
Mzd = u(6);

% LHS of constraint (equal to zero desired)
ellx = abs(ell_R(1)); elly = abs(ell_R(2)); ellb = abs(ell_B(1));
Mx = ( Ct*f1*sin(a1) - Ct*f2*sin(a2))*elly + Cm*f1*cos(a1) - Cm*f2*cos(a2);
My = (-Ct*f1*sin(a1) - Ct*f2*sin(a2))*ellx + Ct*f3*ellb;
Mz =   Cm*f1*sin(a1) - Cm*f2*sin(a2)       - Cm*f3;
Fx =   Ct*f1*cos(a1) + Ct*f2*cos(a2);
Fz =   Ct*f1*sin(a1) + Ct*f2*sin(a2)       + Ct*f3;

% add desired value to constraints
Mx = Mx - Mxd;
My = My - Myd;
Mz = Mz - Mzd;
Fx = Fx - Fxd;
Fz = Fz - Fzd;

F = [Mx My Mz Fx Fz]';
end

function actuators = px4_mixer(u)
% multirotor mode initial actuators
actuators = [0 0 0 pi/2 pi/2];

% u = [F; M]; \in R^6

% from PX4: (in frd/NED). post multiplied by [M; thrust];
% ./px_generate_mixers.py -f ../tri_y.toml --verbose
% B = [[-0.58  0.33  0.  -0.33]
%      [ 0.58  0.33  0.  -0.33]
%      [-0.   -0.67  0.  -0.33]];

% See also
% https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8027494&tag=1

% motor geometry (m)
ell_L = [ 0.080  0.200  0]';
ell_R = [ 0.080 -0.200  0]';
ell_B = [-0.250  0      0]';

% for convergence (actuator effectiveness matrix)
Ct = 0.3*9.81; % coeff of thrust
Cm = 0; % coeff of moment
ellx = abs(ell_R(1)); elly = abs(ell_R(2)); ellb = abs(ell_B(1));
A = [[Ct*elly -Ct*elly  0      ]
     [Ct*ellx  Ct*ellx -Ct*ellb]
     [Cm      -Cm      -Cm     ]
     [Ct       Ct       Ct     ]];
% mixing matrix. in flu/ENU. post multiplied by [M; thrust];
B = pinv(A);

% input saturation
u(4) = sat(u(4), -10, 10);
u(5) = sat(u(5), -10, 10);
u(6) = sat(u(6), -10, 10);

% TRI-Y MIXER (for motors)
delta = B*[u(4);u(5);u(6);u(3)];
omega1 = sat(delta(1), 0, 10);
omega2 = sat(delta(2), 0, 10);
omega3 = sat(delta(3), 0, 10);

% SimpleMixers (for differentially commanded servos)
yawcmd = 0.01*u(6);
yawcmd = 0.1*u(6);

tilt = 0;
servoL = ((tilt* 2)-1) + ((yawcmd*0.8)+0);
servoR = ((tilt*-2)+1) + ((yawcmd*0.8)+0);

servoL = sat(servoL, -1.1, 0);
servoR = sat(servoR,  0, 1.1);


actuators = [omega1 omega2 omega3 servoL*-pi/2 servoR*pi/2];


end

function v = sat(u, min, max)
    if     u>max, v = max;
    elseif u<min, v = min;
    else        , v = u;
    end
end


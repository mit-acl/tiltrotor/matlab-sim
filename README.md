Tiltrotor MATLAB Simulation
===========================

This repo contains the MATLAB simulation files wherein the actuator mixing strategy is developed. Run the `main.m` script to see something happen.

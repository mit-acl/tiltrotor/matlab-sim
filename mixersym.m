clear, clc;

% syms cTf ellx elly ellb cTb;
% 
% A = [cTf elly -cTf -elly     0;...
%      0  -ellx    0 -ellx  ellb;...
%      0   cTf     0 -cTf  -cTb ;...
%      1      0    1     0     0;...
%      0      1    0     1     1];
% 
% % removes Fx.
% A2 = [cTf elly -cTf -elly     0;...
%      0  -ellx    0 -ellx  ellb;...
%      0   cTf     0 -cTf  -cTb ;...
%      0      1    0     1     1];
% 
% simplify(det(A))
% simplify(inv(A))
% 
% % evaluate model using measurements of full-size convergence
% elly = 0.200;
% ellx = 0.080;
% ellb = 0.250;
% cTf = 5;
% cTb = 5;
% M = subs(pinv(A2))
% 
% M = double(M);
% 
% % example
% FM = [0.9; 0; 0; 0.755*9.8]; % Mx, My, Mz, Fz
% f = M*FM
%  
% f1 = norm([f(1) f(2)]) % front left (CW)
% a1 = rad2deg(atan2(f(2),f(1))) % front-left tilt
% f2 = norm([f(3) f(4)]) % front right (CCW)
% a2 = rad2deg(atan2(f(4),f(3))) % front-right tilt
% f3 = f(5) % back (CCW)
% 
% % Fx, Fy, Fz, Mx, My, Mz
% u = [0; 0; FM(4); FM(1); FM(2); FM(3)];


syms Ct Cm elly ellx ellb

B = [Cm Ct*elly  -Cm -Ct*elly    0       ;...
     0 -Ct*ellx   0  -Ct*ellx    Ct*ellb ;...
     0  Cm        0  -Cm        -Cm      ;...
     Ct 0         Ct  0          0       ;...
     0  Ct        0   Ct         Ct      ];
 
simplify(det(B))
simplify(inv(B))

% evaluate model using measurements of full-size convergence
ellx = 0.080;
elly = 0.200;
ellb = 0.250;
Ct = 0.3*9.80665; % convert motor PWM command to force
Cm = 0.3; % rotor drag constant
B = subs(B)
M = pinv(B)

B = double(B);
M = double(M);

%% example: delta to u to m

% f1 f2 f3 alpha1 alpha2
delta = [0 0 0 pi/2 pi/2];

% f1x f1z f2x f2z f3
u = [delta(1)*cos(delta(4)) delta(1)*sin(delta(4))...
     delta(2)*cos(delta(5)) delta(2)*sin(delta(5))...
     delta(3)]';

% Mx My Mz Fx Fz
m = B*u


%% example: m to u

% Fx, Fy, Fz, Mx, My, Mz
FM = [0 0 0.250*9.80665 0 0 0]';

% Mx My Mz Fx Fz
m = [FM(4) FM(5) FM(6) FM(1) FM(3)]';

% f1x f1z f2x f2z f3
u = M*m

f1 = norm([u(1) u(2)]); % front left (CW)
a1 = rad2deg(atan2(u(2),u(1))); % front-left tilt
f2 = norm([u(3) u(4)]); % front right (CCW)
a2 = rad2deg(atan2(u(4),u(3))); % front-right tilt
f3 = u(5); % back (CCW)

% f1 f2 f3 alpha1 alpha2
deltad = [f1 f2 f3 a1 a2]
delta = [f1 f2 f3 deg2rad(a1) deg2rad(a2)]

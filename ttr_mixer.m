function actuators = ttr_mixer(u)
% u = [Fx Fy Fz Mx My Mz]

persistent a1 a2;
if isempty(a1), a1 = deg2rad(0); end
if isempty(a2), a2 = -a1; end

% aerodynamic force and moment coefficients
kf = 4.531e-5; % N/rpm^2
km = 9.409e-7; % N-m/rpm^2

% motor geometry
ellx = 0.080; % m
elly = 0.200; % m
ell3 = 0.250; % m

%% Step 1

% actuator effectiveness matrix for allocating Mx, My, Fz (body flu)
B = [[-kf*elly*cos(a1)-km*sin(a1)  kf*elly*cos(a2)+km*sin(a2) 0]
     [-kf*ellx*cos(a1)            -kf*ellx*cos(a2)            kf*ell3]
     [ kf*cos(a1)                  kf*cos(a2)                 kf]];

% solve for motor speeds squared
delta = B\[u(4);u(5);u(3)];
O1sq = delta(1);
O2sq = delta(2);
O3sq = delta(3);

%% Step 2

% calculate tilt angle to achieve desired yaw moment
a1 = (u(6) - km*(O2sq - O1sq - O3sq))/((O1sq + O2sq)*kf*elly);

% differential tilting in hover flight
a2 = -a1;

%% Output

% motor speed saturation to ensure >0
O1sq = sat(O1sq, 0, inf);
O2sq = sat(O2sq, 0, inf);
O3sq = sat(O3sq, 0, inf);

% servo saturation to ensure small angle
a1 = sat(a1, -pi/20, pi/20);
a2 = sat(a2, -pi/20, pi/20);

% pack up actuators
actuators = [O1sq O2sq O3sq a1 a2];
end

function v = sat(u, min, max)
    if     u>max, v = max;
    elseif u<min, v = min;
    else        , v = u;
    end
end

